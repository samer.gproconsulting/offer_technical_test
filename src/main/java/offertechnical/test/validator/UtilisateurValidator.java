package offertechnical.test.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;

import offertechnical.test.dto.UtilisateurDto;

public class UtilisateurValidator {

	public static List<String> validate(UtilisateurDto dto) {
		List<String> errors = new ArrayList<>();

		if (dto == null) {
			errors.add("Veuillez renseigner le username");
			errors.add("Veuillez renseigner la birthdate");
			errors.add("Veuillez renseigner le country");

			return errors;
		}

		if (!StringUtils.hasLength(dto.getUsername())) {
			errors.add("Veuillez renseigner le username");
		}
		
		//Only adult French residents are allowed to create an account!

		if (dto.getBirthdate() == null) {
			errors.add("Veuillez renseigner la birthdate");
		}else {
			
			if(!adult(dto.getBirthdate())) {
				
				errors.add("Only adult residents are allowed to create an account!");
			}
			
		}

		if (!StringUtils.hasLength(dto.getCountry())) {
			errors.add("Veuillez renseigner le country");
		}else
		{
			
			if (!dto.getCountry().equalsIgnoreCase("France")) {
				errors.add("Only French residents are allowed to create an account!");
			}
		}
		

		

		
		

		return errors;
	}

	private static boolean adult(Date birthdate) {
	     Date today = new Date();
	     
	     if((today.getYear() - birthdate.getYear()) >= 18 )
	     
	        return true;
	     
	     
	     
	     return false ;
	}


}
