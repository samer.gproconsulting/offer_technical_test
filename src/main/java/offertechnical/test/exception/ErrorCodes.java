package offertechnical.test.exception;

public enum ErrorCodes {
	
	
	UTILISATEUR_NOT_FOUND(1111),
	UTILISATEUR_NOT_VALID(1112);
	


  private int code;

  ErrorCodes(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
