package offertechnical.test.service;

import offertechnical.test.dto.UtilisateurDto;

public interface IUtilisateurService {

  UtilisateurDto register(UtilisateurDto dto);

  UtilisateurDto displaysDatails(Long id);

 
}
