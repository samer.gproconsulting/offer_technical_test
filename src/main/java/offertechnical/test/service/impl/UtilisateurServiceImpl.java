package offertechnical.test.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import offertechnical.test.dto.UtilisateurDto;
import offertechnical.test.exception.EntityNotFoundException;
import offertechnical.test.exception.ErrorCodes;
import offertechnical.test.exception.InvalidEntityException;
import offertechnical.test.repository.UtilisateurRepository;
import offertechnical.test.service.IUtilisateurService;
import offertechnical.test.validator.UtilisateurValidator;

@Service

public class UtilisateurServiceImpl implements IUtilisateurService {

	private UtilisateurRepository UtilisateurRepository;

	@Autowired
	public UtilisateurServiceImpl(UtilisateurRepository UtilisateurRepository) {
		this.UtilisateurRepository = UtilisateurRepository;

	}

	@Override
	public UtilisateurDto register(UtilisateurDto dto) {
		List<String> errors = UtilisateurValidator.validate(dto);

		if (!errors.isEmpty()) {
		
			throw new InvalidEntityException("L'Utilisateur n'est pas valide", ErrorCodes.UTILISATEUR_NOT_VALID,
					errors);
		}

		return UtilisateurDto.fromEntity(UtilisateurRepository.save(UtilisateurDto.toEntity(dto)));

	}

	@Override
	public UtilisateurDto displaysDatails(Long id) {
		if (id == null) {
			return null;
		}

		return UtilisateurRepository.findById(id).map(UtilisateurDto::fromEntity)
				.orElseThrow(() -> new EntityNotFoundException(
						"Aucun Utilisateur avec l'ID = " + id + " n' ete trouve dans la BDD",
						ErrorCodes.UTILISATEUR_NOT_FOUND));
	}
}
