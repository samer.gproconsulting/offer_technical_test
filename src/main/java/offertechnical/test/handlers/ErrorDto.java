package offertechnical.test.handlers;


import java.util.ArrayList;
import java.util.List;

import offertechnical.test.exception.ErrorCodes;



public class ErrorDto {

  private Integer httpCode;

  private ErrorCodes code;

  private String message;

  private List<String> errors = new ArrayList<>();

public Integer getHttpCode() {
	return httpCode;
}

public void setHttpCode(Integer httpCode) {
	this.httpCode = httpCode;
}

public ErrorCodes getCode() {
	return code;
}

public void setCode(ErrorCodes code) {
	this.code = code;
}

public String getMessage() {
	return message;
}

public void setMessage(String message) {
	this.message = message;
}

public List<String> getErrors() {
	return errors;
}

public void setErrors(List<String> errors) {
	this.errors = errors;
}

public ErrorDto( ErrorCodes code,Integer httpCode, String message, List<String> errors) {
	super();

	this.code = code;
	this.httpCode = httpCode;
	this.message = message;
	this.errors = errors;
}

  
public ErrorDto( ErrorCodes code,Integer httpCode, String message) {
	super();

	this.code = code;
	this.httpCode = httpCode;
	this.message = message;

}


}
