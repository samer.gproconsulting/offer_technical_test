package offertechnical.test.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import offertechnical.test.exception.EntityNotFoundException;
import offertechnical.test.exception.InvalidEntityException;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<ErrorDto> handleException(EntityNotFoundException exception, WebRequest webRequest) {

		final HttpStatus notFound = HttpStatus.NOT_FOUND;

		final ErrorDto errorDto = new ErrorDto(exception.getErrorCode(), notFound.value(), exception.getMessage());

		return new ResponseEntity<>(errorDto, notFound);
	}

	@ExceptionHandler(InvalidEntityException.class)
	public ResponseEntity<ErrorDto> handleException(InvalidEntityException exception, WebRequest webRequest) {
		final HttpStatus badRequest = HttpStatus.BAD_REQUEST;

		final ErrorDto errorDto = new ErrorDto(exception.getErrorCode(), badRequest.value(), exception.getMessage(),
				exception.getErrors());

		return new ResponseEntity<>(errorDto, badRequest);
	}

}
