package offertechnical.test.dto;

import java.util.Date;

import offertechnical.test.entity.Utilisateur;

public class UtilisateurDto {

	private Long id;
	
	private String nom;

	private String prenom;

	private String username;

	private Date birthdate;

	private String country;

	private String phoneNumber;

	private String gender;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public static Utilisateur toEntity(UtilisateurDto utilisateurDto) {
		if (utilisateurDto == null) {
			return null;
		}

		Utilisateur utilisateur = new Utilisateur();

		utilisateur.setId(utilisateurDto.getId());
		utilisateur.setUsername(utilisateurDto.getUsername());
		utilisateur.setBirthdate(utilisateurDto.getBirthdate());
		utilisateur.setCountry(utilisateurDto.getCountry());
		utilisateur.setGender(utilisateurDto.getGender());
		utilisateur.setPhoneNumber(utilisateurDto.getPhoneNumber());
		
		utilisateur.setNom(utilisateurDto.getNom());
		utilisateur.setPrenom(utilisateurDto.getPrenom());

		return utilisateur;
	}

	public static UtilisateurDto fromEntity(Utilisateur utilisateur) {
		if (utilisateur == null) {
			return null;
		}

		UtilisateurDto utilisateurDto = new UtilisateurDto();

		utilisateurDto.setId(utilisateur.getId());
		utilisateurDto.setUsername(utilisateur.getUsername());
		utilisateurDto.setBirthdate(utilisateur.getBirthdate());
		utilisateurDto.setCountry(utilisateur.getCountry());
		utilisateurDto.setGender(utilisateur.getGender());
		utilisateurDto.setPhoneNumber(utilisateur.getPhoneNumber());
		
		
		utilisateurDto.setNom(utilisateur.getNom());
		utilisateurDto.setPrenom(utilisateur.getPrenom());


		return utilisateurDto;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
	
}
