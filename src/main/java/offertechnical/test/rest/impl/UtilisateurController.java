package offertechnical.test.rest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import offertechnical.test.dto.UtilisateurDto;
import offertechnical.test.rest.UtilisateurApi;
import offertechnical.test.service.IUtilisateurService;

@RestController
public class UtilisateurController implements UtilisateurApi {

	@Autowired
	private IUtilisateurService UtilisateurService;

	@Override
	public ResponseEntity<UtilisateurDto> register(UtilisateurDto utilisateurDto) {

		return new ResponseEntity<UtilisateurDto>(UtilisateurService.register(utilisateurDto), HttpStatus.CREATED);

	}

	@Override
	public ResponseEntity<UtilisateurDto> displaysDetails(Long utilisateurId) {

		return new ResponseEntity<UtilisateurDto>(UtilisateurService.displaysDatails(utilisateurId), HttpStatus.OK);
	}

}
