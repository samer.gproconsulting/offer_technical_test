package offertechnical.test.rest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import offertechnical.test.dto.UtilisateurDto;

@RequestMapping("utilisateurs")
public interface UtilisateurApi {

  @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<UtilisateurDto> register(@RequestBody UtilisateurDto dto);

  @GetMapping(value = "/{idUtilisateur}", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<UtilisateurDto> displaysDetails(@PathVariable("idUtilisateur") Long id);

}
